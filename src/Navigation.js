import React from "react";
import { WithWizard } from "react-albus";

const Navigation = () => (
  <WithWizard
    render={({ next, previous, step, steps }) => (
      <div className="new-register-navigation-buttons">
        {steps.indexOf(step) > 0 && (
          <button
            className="new-register-button-back js-new-register-button-back"
            onClick={previous}
            onNext={window.scrollTo(0, 100)}
          >
            Zurück
          </button>
        )}

        {steps.indexOf(step) < steps.length - 1 && (
          <button
            className="new-register-button-forward js-new-register-button-forward"
            onClick={next}
            onNext={window.scrollTo(0, 100)}
          >
            Weiter
          </button>
        )}
      </div>
    )}
  />
);

export default Navigation;
