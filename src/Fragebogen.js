import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import { CSSTransition } from "react-transition-group";
import { Wizard, Steps, Step } from "react-albus";
import Navigation from "./Navigation";
import ProgressBar from "./ProgressBar";

import "./exampleAnimation.css";

// Frage 1
import gemuetlich from "./gemuetlich.png";
import sportlich from "./sportlich.png";
import muede from "./muede.png";
import immer_anders from "./immer_anders.png";

// Frage 2
import play_hard from "./play_hard.png";
import spaet_als_nie from "./spaet_als_nie.png";
import carpe_diem from "./carpe_diem.png";
import ohne_moos from "./ohne_moos.png";

// Frage 3
import abenteurer from "./abenteurer.jpg";
import computernerd from "./computernerd.jpg";
import familienmensch from "./familienmensch.jpg";
import fitnessfan from "./fitnessfan.jpg";
import holzfaeller from "./holzfaeller.jpg";
import relaxer from "./relaxer.jpg";
import rockstar from "./rockstar.jpg";
import taenzer from "./taenzerin.jpg";

// Frage 4
import city from "./citygirlboy.jpg";
import extremsportler from "./extremsportler.jpg";
import kulturliebhaber from "./kulturliebhaber.jpg";
import partygaenger from "./partygaenger.jpg";
import sonnenanbeter from "./sonnenanbeter.jpg";
import geniesser from "./geniesser.jpg";
import wanderer from "./wanderlust.jpg";
import balkonien from "./balkonien.jpg";

// Frage 5
import afrika from "./afrika.jpg";
import amerika from "./amerika.jpg";
import asien from "./asien.jpg";
import deutschland from "./deutschland.jpg";
import england from "./england.jpg";
import mittelmeer from "./mittelmeer.jpg";
import indien from "./indien.jpg";
import mexiko from "./mexiko.jpg";

// Frage 6
import sportskanone from "./sportskanone.png";
import couchpotato from "./couchpotato.png";
import actionfan from "./actionfan.png";
import natur_tierfreund from "./naturfreund.png";

// Frage 7
import ei from "./ei.jpg";
import fruchtaufstrich from "./fruchtaufstrich.jpg";
import gemuese from "./gemuese.jpg";
import herzhaftes from "./herzhaftes.jpg";
import muesli from "./muesli.jpg";
import obst from "./obst.jpg";
import sandwich from "./sandwich.jpg";
import suess from "./suess.jpg";

// Frage 8
import anregend from "./anregend.jpg";
import beruhigend from "./beruhigend.jpg";
import cremig from "./cremig.jpg";
import fruchtig from "./fruchtig.jpg";

// Frage 9
import vegan from "./vegan.png";
import vegetarisch from "./vegetarisch.png";
import paleo from "./paleo.png";
import low_carb from "./low_carb.png";

// Frage 10
import asiatisch from "./asiatisch.jpg";
import eiauftoast from "./eiauftoast.jpg";
import fischig from "./fischig.jpg";
import fruchtig_nogoes from "./fruchtig_nogoes.jpg";
import mexikanisch from "./mexikanisch.jpg";
import pancakes from "./pancakes.jpg";
import schimmelkaese from "./schimmelkaese.jpg";
import wuerzig from "./wuerzig.jpg";

// Frage 11
import eier from "./eier.jpg";
import erdnuesse from "./erdnuesse.jpg";
import fisch from "./fisch.jpg";
import gluten from "./gluten.jpg";
import huelsenfruechte from "./huelsenfruechte.jpg";
import krebstiere from "./krebstiere.jpg";
import milch from "./milch.jpg";
import senf from "./senf.jpg";

// Frage 12
import mama_machts from "./mama_machts.png";
import hobbykoch from "./hobbykoch.png";
import tiefkuehl_spezialist from "./tiefkuehl_spezialist.png";
import chefkoch from "./chefkoch.png";

// Frage 13
import minuten_5 from "./5minuten.svg";
import minuten_15 from "./15minuten.svg";
import minuten_30 from "./30minuten.svg";
import minuten_60 from "./60minuten.svg";

// Frage 14
import cake_1 from "./cake_1.svg";
import cake_2 from "./cake_2.svg";
import cake_3 from "./cake_3.svg";
import cake_4 from "./cake_4.svg";

// Frage 15
import inspiration from "./inspiration.png";
import neugierig from "./neugierig.png";
import zeit_sparen from "./zeit_sparen.png";
import persoenlich from "./persoenlich.png";

// Frage 16
import coins_1 from "./coins_1.svg";
import coins_2 from "./coins_2.svg";
import coins_3 from "./coins_3.svg";
import coins_4 from "./coins_4.svg";

// Frage 17
import message from "./message.svg";

class Fragebogen extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCheckboxChange = e => {
    let checkboxname = e.target.name;
    let checkbox = document.getElementById(checkboxname);

    if (checkbox.checked == true) {
      this.setState({ [e.target.name]: e.target.value });
    } else {
      this.setState({ [e.target.name]: null });
    }
  };

  render() {
    return (
      <BrowserRouter>
        <div className="new-register-wrapper">
          <div className="">
            <Wizard
              onNext={window.scrollTo(50, 0)}
              render={({ step, steps }) => (
                <React.Fragment>
                   <ProgressBar
                                  circle1Progress={
                                    (steps.indexOf(step) / 6) * 100
                                  }
                                  line1Progress={
                                    steps.indexOf(step) / 6 >= 1 ? 100 : 0
                                  }
                                  circle2Progress={
                                    steps.indexOf(step) >= 7
                                      ? ((steps.indexOf(step) - 6) / 7) * 100
                                      : 0
                                  }
                                  line2Progress={
                                    steps.indexOf(step) / 13 >= 1 ? 100 : 0
                                  }
                                  circle3Progress={
                                    steps.indexOf(step) >= 13
                                      ? ((steps.indexOf(step) - 12) / 5) * 100
                                      : 0
                                  }
                                />

                  <CSSTransition
                    key={step.id}
                    classNames="example"
                    timeout={{ enter: 500, exit: 500 }}
                  >
                    <div className="">
                      <Steps key={step.id} step={step}>
                        <Step id="Step1">
                          <div className="new-register-question-context">
                            <h1>Wie startet dein Tag ohne Frühstart?</h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={gemuetlich}
                                  alt="Gemütlich"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_1_fruh1"
                                  name="lifestyle_1_fruh"
                                  value="{[1, 0, 0 ,0]}"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_1_fruh1"
                                  title="Gemütlich"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Gemütlich
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={sportlich}
                                  alt="Sportlich"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_1_fruh2"
                                  name="lifestyle_1_fruh"
                                  value="{[0, 0, 2, 0]}"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_1_fruh2"
                                  title="Sportlich"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Sportlich
                                      <span className="clickable-input_emphasize" />
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={muede}
                                  alt="Müde"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_1_fruh3"
                                  name="lifestyle_1_fruh"
                                  value="{[0, 1, 0, 0]}"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_1_fruh3"
                                  title="Müde"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Müde
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={immer_anders}
                                  alt="Immer anders"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_1_fruh4"
                                  name="lifestyle_1_fruh"
                                  value="{[0, 0, 0, 1]}"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_1_fruh4"
                                  title="Immer anders"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Immer anders
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step2">
                          <div className="new-register-question-context">
                            <h1>Was ist dein Motto?</h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={play_hard}
                                  alt="Work Hard. Play Hard."
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_2_fruh1"
                                  name="lifestyle_2_fruh"
                                  value="{[0, 0, 1, 0]}"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_2_fruh1"
                                  title="Work Hard. Play Hard."
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Work Hard. Play Hard.
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={spaet_als_nie}
                                  alt="Lieber spät als nie."
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_2_fruh2"
                                  name="lifestyle_2_fruh"
                                  value="{[0, 1, 0, 0]}"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_2_fruh2"
                                  title="Lieber spät als nie."
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Lieber spät als nie.
                                      <span className="clickable-input_emphasize" />
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={carpe_diem}
                                  alt="Carpe Diem."
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_2_fruh3"
                                  name="lifestyle_2_fruh"
                                  value="{[0, 0, 0, 2]}"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_2_fruh3"
                                  title="Carpe Diem."
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Carpe Diem.
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={ohne_moos}
                                  alt="Ohne Moos nix los."
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_2_fruh4"
                                  name="lifestyle_2_fruh"
                                  value="{[2, 0, 0, 0]}"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_2_fruh4"
                                  title="Ohne Moos nix los."
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Ohne Moos nix los.
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step3">
                          <div className="new-register-question-context">
                            <h1>Welches Bild beschreibt dich am ehesten?</h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_3_fruh1"
                                  name="lifestyle_3_fruh1"
                                  value="{[0, 0, 0, 2]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_3_fruh1"
                                  title="Abenteurer"
                                  tabIndex={0}
                                >
                                  <img src={abenteurer} alt="Abenteurer" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_3_fruh2"
                                  name="lifestyle_3_fruh2"
                                  value="{[1, 0, 0, 0]}"
                                  onClick={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_3_fruh2"
                                  title="Computernerd"
                                  tabIndex={0}
                                >
                                  <img src={computernerd} alt="Computernerd" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_3_fruh3"
                                  name="lifestyle_3_fruh3"
                                  value="{[0, 2, 0, 0]}"
                                  onClick={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_3_fruh3"
                                  title="Familienmensch"
                                  tabIndex={0}
                                >
                                  <img
                                    src={familienmensch}
                                    alt="Familienmensch"
                                  />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_3_fruh4"
                                  name="lifestyle_3_fruh4"
                                  value="{[0, 0, 2, 0]}"
                                  onClick={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_3_fruh4"
                                  title="Fitnessfan"
                                  tabIndex={0}
                                >
                                  <img src={fitnessfan} alt="Fitnessfan" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_3_fruh5"
                                  name="lifestyle_3_fruh5"
                                  value="{[0, 0, 0, 1]}"
                                  onClick={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_3_fruh5"
                                  title="Holzfäller"
                                  tabIndex={0}
                                >
                                  <img src={holzfaeller} alt="Holzfäller" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_3_fruh6"
                                  name="lifestyle_3_fruh6"
                                  value="{[0, 1, 0, 0]}"
                                  onClick={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_3_fruh6"
                                  title="Holzfäller"
                                  tabIndex={0}
                                >
                                  <img src={relaxer} alt="Relaxer" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_3_fruh7"
                                  name="lifestyle_3_fruh7"
                                  value="{[1, 0, 0, 0]}"
                                  onClick={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_3_fruh7"
                                  title="Relaxer"
                                  tabIndex={0}
                                >
                                  <img src={rockstar} alt="Rockstar" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_3_fruh8"
                                  name="lifestyle_3_fruh8"
                                  value="{[0, 0, 1, 0]}"
                                  onClick={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_3_fruh8"
                                  title="Dummy"
                                  tabIndex={0}
                                >
                                  <img src={taenzer} alt="Tänzer" />
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step4">
                          <div className="new-register-question-context">
                            <h1>Welcher Urlaubstyp bist du?</h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_4_fruh1"
                                  name="lifestyle_4_fruh1"
                                  value="{[1, 0, 0, 0]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_4_fruh1"
                                  title="City Girl/Boy"
                                  tabIndex={0}
                                >
                                  <img src={city} alt="City Girl/Boy" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_4_fruh2"
                                  name="lifestyle_4_fruh2"
                                  value="{[0, 0, 1, 2]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_4_fruh2"
                                  title="Extremsportler"
                                  tabIndex={0}
                                >
                                  <img
                                    src={extremsportler}
                                    alt="Extremsportler"
                                  />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_4_fruh3"
                                  name="lifestyle_4_fruh3"
                                  value="{[2, 0, 0, 0]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_4_fruh3"
                                  title="Kulturliebhaber"
                                  tabIndex={0}
                                >
                                  <img
                                    src={kulturliebhaber}
                                    alt="Kulturliebhaber"
                                  />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_4_fruh4"
                                  name="lifestyle_4_fruh4"
                                  value="{[0, 0, 2, 0]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_4_fruh4"
                                  title="Partygänger"
                                  tabIndex={0}
                                >
                                  <img src={partygaenger} alt="Partygänger" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_4_fruh5"
                                  name="lifestyle_4_fruh5"
                                  value="{[0, 0, 2, 0]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_4_fruh5"
                                  title="Partygänger"
                                  tabIndex={0}
                                >
                                  <img src={geniesser} alt="Genießer" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_4_fruh6"
                                  name="lifestyle_4_fruh6"
                                  value="{[0, 1, 0 ,0]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_4_fruh6"
                                  title="Sonnenanbeter"
                                  tabIndex={0}
                                >
                                  <img
                                    src={sonnenanbeter}
                                    alt="Sonnenanbeter"
                                  />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_4_fruh7"
                                  name="lifestyle_4_fruh7"
                                  value="{[0, 0, 0, 1]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_4_fruh7"
                                  title="Wanderer"
                                  tabIndex={0}
                                >
                                  <img src={wanderer} alt="Wanderer" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_4_fruh8"
                                  name="lifestyle_4_fruh8"
                                  value="{[0, 1, 0, 0]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_4_fruh8"
                                  title="Balkonien"
                                  tabIndex={0}
                                >
                                  <img src={balkonien} alt="Balkonien" />
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step5">
                          <div className="new-register-question-context">
                            <h1>Wo schmeckt es dir am besten?</h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_5_fruh1"
                                  name="lifestyle_5_fruh1"
                                  value="{[1, 0, 0, 1]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_5_fruh1"
                                  title="Afrika"
                                  tabIndex={0}
                                >
                                  <img src={afrika} alt="Afrika" />
                                  <span className="clickable-input_subline">
                                    Afrika
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_5_fruh2"
                                  name="lifestyle_5_fruh2"
                                  value="{[0, 1, 1, 0]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_5_fruh2"
                                  title="Amerika"
                                  tabIndex={0}
                                >
                                  <img src={amerika} alt="Amerika" />
                                  <span className="clickable-input_subline">
                                    Amerika
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_5_fruh3"
                                  name="lifestyle_5_fruh3"
                                  value="{[1, 0, 0, 1]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_5_fruh3"
                                  title="Asien"
                                  tabIndex={0}
                                >
                                  <img src={asien} alt="Asien" />
                                  <span className="clickable-input_subline">
                                    Asien
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_5_fruh4"
                                  name="lifestyle_5_fruh4"
                                  value="{[0, 1, 0, 0]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_5_fruh4"
                                  title="Deutschland"
                                  tabIndex={0}
                                >
                                  <img src={deutschland} alt="Deutschland" />
                                  <span className="clickable-input_subline">
                                    Deutschland
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_5_fruh5"
                                  name="lifestyle_5_fruh5"
                                  value="{[0, 0, 1, 0]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_5_fruh5"
                                  title="England"
                                  tabIndex={0}
                                >
                                  <img src={england} alt="England" />
                                  <span className="clickable-input_subline">
                                    England
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_5_fruh6"
                                  name="lifestyle_5_fruh6"
                                  value="{[1, 0, 0, 1]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_5_fruh6"
                                  title="Mittelmeer"
                                  tabIndex={0}
                                >
                                  <img src={mittelmeer} alt="Mittelmeer" />
                                  <span className="clickable-input_subline">
                                    Mittelmeer
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_5_fruh7"
                                  name="lifestyle_5_fruh7"
                                  value="{[1, 0, 1, 1]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_5_fruh7"
                                  title="Indien"
                                  tabIndex={0}
                                >
                                  <img src={indien} alt="Indien" />
                                  <span className="clickable-input_subline">
                                    Indien
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_5_fruh8"
                                  name="lifestyle_5_fruh8"
                                  value="{[0, 0, 1, 1]}"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_5_fruh8"
                                  title="Mexiko"
                                  tabIndex={0}
                                >
                                  <img src={mexiko} alt="Mexiko" />
                                  <span className="clickable-input_subline">
                                    Mexiko
                                  </span>
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step6">
                          <div className="new-register-question-context">
                            <h1>Welcher Freizeit-Typ bist du?</h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={sportskanone}
                                  alt="Sportskanone"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_6_fruh1"
                                  name="lifestyle_6_fruh"
                                  value="{[0, 0, 2, 0]}"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_6_fruh1"
                                  title="Sportskanone"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Sportskanone
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={couchpotato}
                                  alt="Couchpotato"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_6_fruh2"
                                  name="lifestyle_6_fruh"
                                  value="{[1, 1, 0, 0]}"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_6_fruh2"
                                  title="Couchpotato"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Couchpotato
                                      <span className="clickable-input_emphasize" />
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={actionfan}
                                  alt="Actionfan"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_6_fruh3"
                                  name="lifestyle_6_fruh"
                                  value="{[0, 0, 1, 2]}"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_6_fruh3"
                                  title="Actionfan"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Actionfan
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={natur_tierfreund}
                                  alt="Natur- und Tierfreund"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_6_fruh4"
                                  name="lifestyle_6_fruh"
                                  value="{[1, 1, 0, 1]}"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_6_fruh4"
                                  title="Natur- und Tierfreund"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Natur- und Tierfreund
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step7">
                          <div className="new-register-question-context">
                            <h1>Was isst du gerne zum Frühstück?</h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_7_fruh1"
                                  name="preferences_7_fruh1"
                                  value="Ei"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_7_fruh1"
                                  title="Ei"
                                  tabIndex={0}
                                >
                                  <img src={ei} alt="Ei" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_7_fruh2"
                                  name="preferences_7_fruh2"
                                  value="Fruchtaufstrich"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_7_fruh2"
                                  title="Fruchtaufstrich"
                                  tabIndex={0}
                                >
                                  <img
                                    src={fruchtaufstrich}
                                    alt="Fruchtaufstrich"
                                  />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_7_fruh3"
                                  name="preferences_7_fruh3"
                                  value="Gemuese"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_7_fruh3"
                                  title="Gemüse"
                                  tabIndex={0}
                                >
                                  <img src={gemuese} alt="Gemüse" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_7_fruh4"
                                  name="preferences_7_fruh4"
                                  value="Herzhaftes"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_7_fruh4"
                                  title="Herzhaftes"
                                  tabIndex={0}
                                >
                                  <img src={herzhaftes} alt="Herzhaftes" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_7_fruh5"
                                  name="preferences_7_fruh5"
                                  value="Muesli"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_7_fruh5"
                                  title="Müsli"
                                  tabIndex={0}
                                >
                                  <img src={muesli} alt="Müsli" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_7_fruh6"
                                  name="preferences_7_fruh6"
                                  value="Obst"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_7_fruh6"
                                  title="Obst"
                                  tabIndex={0}
                                >
                                  <img src={obst} alt="Obst" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_7_fruh7"
                                  name="preferences_7_fruh7"
                                  value="Sandwich"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_7_fruh7"
                                  title="Sandwich"
                                  tabIndex={0}
                                >
                                  <img src={sandwich} alt="Sandwich" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_7_fruh8"
                                  name="preferences_7_fruh8"
                                  value="Suess"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_7_fruh8"
                                  title="Suess"
                                  tabIndex={0}
                                >
                                  <img src={suess} alt="Süß" />
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step8">
                          <div className="new-register-question-context">
                            <h1>Was trinkst du gerne?</h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_8_fruh1"
                                  name="preferences_8_fruh1"
                                  value="anregend"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_8_fruh1"
                                  title="anregend"
                                  tabIndex={0}
                                >
                                  <img src={anregend} alt="anregend" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_8_fruh2"
                                  name="preferences_8_fruh2"
                                  value="beruhigend"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_8_fruh2"
                                  title="beruhigend"
                                  tabIndex={0}
                                >
                                  <img src={beruhigend} alt="beruhigend" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_8_fruh3"
                                  name="preferences_8_fruh3"
                                  value="cremig"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_8_fruh3"
                                  title="cremig"
                                  tabIndex={0}
                                >
                                  <img src={cremig} alt="cremig" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_8_fruh4"
                                  name="preferences_8_fruh4"
                                  value="fruchtig"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_8_fruh4"
                                  title="fruchtig"
                                  tabIndex={0}
                                >
                                  <img src={fruchtig} alt="fruchtig" />
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step9">
                          <div className="new-register-question-context">
                            <h1>
                              Befolgst du eine der folgenden Ernährungsformen?
                            </h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={vegan}
                                  alt="Vegan"
                                />
                                <input
                                  type="radio"
                                  id="preferences_9_fruh1"
                                  name="preferences_9_fruh"
                                  value="Vegan"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_9_fruh1"
                                  title="Vegan"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Vegan
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={vegetarisch}
                                  alt="Vegetarisch"
                                />
                                <input
                                  type="radio"
                                  id="fpreferences_9_fruh2"
                                  name="preferences_9_fruh"
                                  value="Vegetarisch"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="fpreferences_9_fruh2"
                                  title="Vegetarisch"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Vegetarisch
                                      <span className="clickable-input_emphasize" />
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={paleo}
                                  alt="Paleo"
                                />
                                <input
                                  type="radio"
                                  id="preferences_9_fruh3"
                                  name="preferences_9_fruh"
                                  value="Paleo"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_9_fruh3"
                                  title="Paleo"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Paleo
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={low_carb}
                                  alt="Low-Carb"
                                />
                                <input
                                  type="radio"
                                  id="preferences_9_fruh4"
                                  name="preferences_9_fruh"
                                  value="Low-Carb"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_9_fruh4"
                                  title="Low-Carb"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Low-Carb
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step10">
                          <div className="new-register-question-context">
                            <h1>Was magst du eher nicht?</h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_10_fruh1"
                                  name="preferences_10_fruh1"
                                  value="Asiatisch"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_10_fruh1"
                                  title="Asiatisch"
                                  tabIndex={0}
                                >
                                  <img src={asiatisch} alt="Asiatisch" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_10_fruh2"
                                  name="preferences_10_fruh2"
                                  value="Ei auf Toast"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_10_fruh2"
                                  title="Ei auf Toast"
                                  tabIndex={0}
                                >
                                  <img src={eiauftoast} alt="Ei auf Toast" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_10_fruh3"
                                  name="preferences_10_fruh3"
                                  value="Fischig"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_10_fruh3"
                                  title="Fischig"
                                  tabIndex={0}
                                >
                                  <img src={fischig} alt="Fischig" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_10_fruh4"
                                  name="preferences_10_fruh4"
                                  value="Fruchtig"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_10_fruh4"
                                  title="Fruchtig"
                                  tabIndex={0}
                                >
                                  <img src={fruchtig_nogoes} alt="Fruchtig" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_10_fruh5"
                                  name="preferences_10_fruh5"
                                  value="Mexikanisch"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_10_fruh5"
                                  title="Mexikanisch"
                                  tabIndex={0}
                                >
                                  <img src={mexikanisch} alt="Mexikanisch" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_10_fruh6"
                                  name="preferences_10_fruh6"
                                  value="Pancakes"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_10_fruh6"
                                  title="Pancakes"
                                  tabIndex={0}
                                >
                                  <img src={pancakes} alt="Pancakes" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_10_fruh7"
                                  name="preferences_10_fruh7"
                                  value="Schimmelkäse"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_10_fruh7"
                                  title="Schimmelkäse"
                                  tabIndex={0}
                                >
                                  <img src={schimmelkaese} alt="Schimmelkäse" />
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="preferences_10_fruh8"
                                  name="preferences_10_fruh8"
                                  value="Würzig"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="preferences_10_fruh8"
                                  title="Würzig"
                                  tabIndex={0}
                                >
                                  <img src={wuerzig} alt="Würzig" />
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step11">
                          <div className="new-register-question-context">
                            <h1>
                              Bist du auf eines oder mehrere der folgenden Dinge
                              allergisch?
                            </h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_11_fruh1"
                                  name="lifestyle_11_fruh1"
                                  value="Eier"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_11_fruh1"
                                  title="Eier"
                                  tabIndex={0}
                                >
                                  <img src={eier} alt="Eier" />
                                  <span className="clickable-input_subline">
                                    Eier
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_11_fruh2"
                                  name="lifestyle_11_fruh2"
                                  value="Hülsenfrüchte"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_11_fruh2"
                                  title="Hülsenfrüchte"
                                  tabIndex={0}
                                >
                                  <img
                                    src={huelsenfruechte}
                                    alt="Hülsenfrüchte"
                                  />
                                  <span className="clickable-input_subline">
                                    Hülsenfrüchte
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_11_fruh3"
                                  name="lifestyle_11_fruh3"
                                  value="Fisch"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_11_fruh3"
                                  title="Fisch"
                                  tabIndex={0}
                                >
                                  <img src={fisch} alt="Fisch" />
                                  <span className="clickable-input_subline">
                                    Fisch
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_11_fruh4"
                                  name="lifestyle_11_fruh4"
                                  value="Gluten"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_11_fruh4"
                                  title="Gluten"
                                  tabIndex={0}
                                >
                                  <img src={gluten} alt="Gluten" />
                                  <span className="clickable-input_subline">
                                    Gluten
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_11_fruh5"
                                  name="lifestyle_11_fruh5"
                                  value="Krebs-/ Weichtiere"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_11_fruh5"
                                  title="Krebs-/ Weichtiere"
                                  tabIndex={0}
                                >
                                  <img
                                    src={krebstiere}
                                    alt="Krebs-/ Weichtiere"
                                  />
                                  <span className="clickable-input_subline">
                                    Krebs-/ Weichtiere
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_11_fruh6"
                                  name="lifestyle_11_fruh6"
                                  value="Milch"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_11_fruh6"
                                  title="Milch"
                                  tabIndex={0}
                                >
                                  <img src={milch} alt="Milch" />
                                  <span className="clickable-input_subline">
                                    Milch
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_11_fruh7"
                                  name="lifestyle_11_fruh7"
                                  value="Erdnüsse"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_11_fruh7"
                                  title="Erdnüsse"
                                  tabIndex={0}
                                >
                                  <img src={erdnuesse} alt="Erdnüsse" />
                                  <span className="clickable-input_subline">
                                    Erdnüsse
                                  </span>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <input
                                  type="checkbox"
                                  id="lifestyle_11_fruh8"
                                  name="lifestyle_11_fruh8"
                                  value="Senf"
                                  onChange={this.handleCheckboxChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_11_fruh8"
                                  title="Senf"
                                  tabIndex={0}
                                >
                                  <img src={senf} alt="Senf" />
                                  <span className="clickable-input_subline">
                                    Senf
                                  </span>
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step12">
                          <div className="new-register-question-context">
                            <h1>Welcher Koch-Typ bist du?</h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={mama_machts}
                                  alt="Mama macht das schon"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_12_fruh1"
                                  name="lifestyle_12_fruh"
                                  value="Mama macht das schon"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_12_fruh1"
                                  title="Mama macht das schon"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Mama macht das schon
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={tiefkuehl_spezialist}
                                  alt="Tiefkühlspezialist"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_12_fruh2"
                                  name="lifestyle_12_fruh"
                                  value="Tiefkühlspezialist"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_12_fruh2"
                                  title="Ich achte auf Qualität und Budget"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Tiefkühl-spezialist
                                      <span className="clickable-input_emphasize" />
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={hobbykoch}
                                  alt="Amateur"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_12_fruh3"
                                  name="lifestyle_12_fruh"
                                  value="Amateur"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_12_fruh3"
                                  title="Amateur"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Amateur
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={chefkoch}
                                  alt="Chefkoch"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_12_fruh4"
                                  name="lifestyle_12_fruh"
                                  value="Chefkoch"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_12_fruh4"
                                  title="Chefkoch"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Chefkoch
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step13">
                          <div className="new-register-question-context">
                            <h1>
                              Wie viel Zeit nimmst du dir für die Zubereitung
                              deines Frühstücks?
                            </h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={minuten_5}
                                  alt="< 5 Minuten"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_13_fruh1"
                                  name="lifestyle_13_fruh"
                                  value="< 5 Minuten"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_13_fruh1"
                                  title="< 5"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      &lt; 5 <br />
                                      Minuten{" "}
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={minuten_15}
                                  alt="5 - 15 Minuten"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_13_fruh2"
                                  name="lifestyle_13_fruh"
                                  value="5 - 15 Minuten"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_13_fruh2"
                                  title="5 - 15 Minuten"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      5-15 <br />
                                      Minuten
                                      <span className="clickable-input_emphasize" />
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={minuten_30}
                                  alt="15 - 30 Minuten"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_13_fruh3"
                                  name="lifestyle_13_fruh"
                                  value="15 - 30 Minuten"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_13_fruh3"
                                  title="15 - 30 Minuten"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      15-30 <br />
                                      Minuten
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={minuten_60}
                                  alt="60 > Minuten"
                                />
                                <input
                                  type="radio"
                                  id="lifestyle_13_fruh4"
                                  name="lifestyle_13_fruh"
                                  value="60 > Minuten"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="lifestyle_13_fruh4"
                                  title="60 > Minuten"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      60 &gt;
                                      <br />
                                      Minuten
                                      <span className="clickable-input_emphasize" />
                                    </span>
                                  </div>
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step14">
                          <div className="new-register-question-context">
                            <h1>Mein gefühltes Alter:</h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={cake_1}
                                  alt="< 20 Jahre alt"
                                />
                                <input
                                  type="radio"
                                  id="finaltuning_14_fruh1"
                                  name="finaltuning_14_fruh"
                                  value="< 20 Jahre alt"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="finaltuning_14_fruh1"
                                  title="< 20 Jahre alt"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      &lt; 20 <br />
                                      Jahre alt
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={cake_2}
                                  alt="20-30 Jahre alt"
                                />
                                <input
                                  type="radio"
                                  id="finaltuning_14_fruh2"
                                  name="finaltuning_14_fruh"
                                  value="20-30 Jahre alt"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="finaltuning_14_fruh2"
                                  title="20-30 Jahre alt"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      20-30 <br />
                                      Jahre alt
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={cake_3}
                                  alt="30-40 Jahre alt"
                                />
                                <input
                                  type="radio"
                                  id="finaltuning_14_fruh3"
                                  name="finaltuning_14_fruh"
                                  value="30-40 Jahre alt"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="finaltuning_14_fruh3"
                                  title="30-40 Jahre alt"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      30-40 <br />
                                      Jahre alt
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={cake_4}
                                  alt="40 > Jahre alt"
                                />
                                <input
                                  type="radio"
                                  id="finaltuning_14_fruh4"
                                  name="finaltuning_14_fruh"
                                  value="40 > Jahre alt"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="finaltuning_14_fruh4"
                                  title="40 > Jahre alt"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      40 > <br />
                                      Jahre alt
                                    </span>
                                  </div>
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step15">
                          <div className="new-register-question-context">
                            <h1>Warum möchtest du unseren Service nutzen?</h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={zeit_sparen}
                                  alt="Ich möchte Zeit sparen"
                                />
                                <input
                                  type="radio"
                                  id="finaltuning_15_fruh1"
                                  name="finaltuning_15_fruh"
                                  value="Ich möchte Zeit sparen"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="finaltuning_15_fruh1"
                                  title="Ich möchte Zeit sparen"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Ich möchte <br />
                                      <span className="clickable-input_emphasize">
                                        Zeit sparen
                                      </span>
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={persoenlich}
                                  alt="Ich möchte ein persönliches Frühstück"
                                />
                                <input
                                  type="radio"
                                  id="finaltuning_15_fruh2"
                                  name="finaltuning_15_fruh"
                                  value="Ich möchte ein persönliches Frühstück"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="finaltuning_15_fruh2"
                                  title="Ich möchte ein persönliches Frühstück"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Ich möchte ein <br />{" "}
                                      <span className="clickable-input_emphasize" />
                                      <span className="clickable-input_emphasize">
                                        persönliches Frühstück
                                      </span>
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={inspiration}
                                  alt="Ich suche Inspiration"
                                />
                                <input
                                  type="radio"
                                  id="finaltuning_15_fruh3"
                                  name="finaltuning_15_fruh"
                                  value="Ich suche Inspiration"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="finaltuning_15_fruh3"
                                  title="Ich suche Inspiration"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Ich <br />
                                      <span className="clickable-input_emphasize">
                                        suche Inspiration
                                      </span>
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={neugierig}
                                  alt="Ich bin neugierig auf den Service"
                                />
                                <input
                                  type="radio"
                                  id="finaltuning_15_fruh4"
                                  name="finaltuning_15_fruh"
                                  value="Ich bin neugierig auf den Service"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="finaltuning_15_fruh4"
                                  title="Ich bin neugierig auf den Service"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Ich bin <br />
                                      <span className="clickable-input_emphasize">
                                        neugierig auf den Service
                                      </span>
                                    </span>
                                  </div>
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step16">
                          <div className="new-register-question-context">
                            <h1>Mein Budget:</h1>
                            <div className="clickable-input-wrapper">
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={coins_1}
                                  alt="Ich habe ein festes Budget"
                                />
                                <input
                                  type="radio"
                                  id="finaltuning_16_fruh1"
                                  name="finaltuning_16_fruh"
                                  value="Ich habe ein festes Budget"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="finaltuning_16_fruh1"
                                  title="Ich habe ein festes Budget"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Ich habe <br /> ein{" "}
                                      <span className="clickable-input_emphasize">
                                        festes Budget
                                      </span>
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={coins_2}
                                  alt="Ich achte auf Qualität und Budget"
                                />
                                <input
                                  type="radio"
                                  id="finaltuning_16_fruh2"
                                  name="finaltuning_16_fruh"
                                  value="Ich achte auf Qualität und Budget"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="finaltuning_16_fruh2"
                                  title="Ich achte auf Qualität und Budget"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Ich achte auf <br />
                                      <span className="clickable-input_emphasize">
                                        Qualität
                                      </span>{" "}
                                      und{" "}
                                      <span className="clickable-input_emphasize">
                                        Budget
                                      </span>
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={coins_3}
                                  alt="Ich bin bereit in Qualität zu investieren"
                                />
                                <input
                                  type="radio"
                                  id="finaltuning_16_fruh3"
                                  name="finaltuning_16_fruh"
                                  value="Ich bin bereit in Qualität zu investieren"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="finaltuning_16_fruh3"
                                  title="Ich bin bereit in Qualität zu investieren"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      Ich bin bereit in <br />
                                      <span className="clickable-input_emphasize">
                                        Qualität
                                      </span>{" "}
                                      zu investieren
                                    </span>
                                  </div>
                                </label>
                              </div>
                              <div className="clickable-input js-clickable-input">
                                <img
                                  className="clickable-input_header"
                                  src={coins_4}
                                  alt="Ich bin neugierig auf den Service"
                                />
                                <input
                                  type="radio"
                                  id="finaltuning_16_fruh4"
                                  name="finaltuning_16_fruh"
                                  value="No Limits! Food is Passion"
                                  onChange={this.handleInputChange}
                                />
                                <label
                                  className="js-clickable-input-label"
                                  htmlFor="finaltuning_16_fruh4"
                                  title="No Limits! Food is Passion"
                                  tabIndex={0}
                                >
                                  <div className="clickable-input_text">
                                    <span>
                                      No Limits! <br />
                                      <span className="clickable-input_emphasize">
                                        Food is Passion
                                      </span>
                                    </span>
                                  </div>
                                </label>
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step17">
                          <div className="new-register-question-context">
                            <h1>
                              Möchtest du uns noch etwas wichtiges mitteilen?
                            </h1>
                            <img
                              className="clickable-input_header"
                              src={message}
                              alt="Ich habe ein festes Budget"
                            />
                            <div className="form-input js-registration-input">
                              <div className="input-wrapper js-input-wrapper">
                                <textarea
                                  type="text"
                                  id="finaltuning_17_fruh1"
                                  name="finaltuning_17_fruh"
                                  required
                                  className="textfield"
                                  value={this.state.finaltuning_17_fruh1}
                                  onChange={this.handleInputChange}
                                />
                              </div>
                            </div>
                          </div>
                        </Step>
                        <Step id="Step18">
                          <div className="new-register-question-context">
                            <h1>Profil speichern</h1>
                            <div className="form-input js-registration-input">
                              <div className="input-wrapper js-input-wrapper">
                                <label
                                  htmlFor="fruehstart_appbundle_userprofilerandregtype_email"
                                  className="required"
                                >
                                  E-Mail Adresse:
                                </label>
                                <input
                                  type="email"
                                  id="fruehstart_appbundle_userprofilerandregtype_email"
                                  name="fruehstart_appbundle_userprofilerandregtype[email]"
                                  required="required"
                                  className="js-register-email"
                                />
                              </div>
                              <div className="input-wrapper js-input-wrapper">
                                <label
                                  htmlFor="fruehstart_appbundle_userprofilerandregtype_password"
                                  className="required"
                                >
                                  Passwort:
                                </label>
                                <div className="input-password">
                                  <input
                                    type="password"
                                    id="fruehstart_appbundle_userprofilerandregtype_password"
                                    name="fruehstart_appbundle_userprofilerandregtype[password]"
                                    required="required"
                                    className="js-register-password"
                                  />
                                </div>
                                <span className="password-eye-icon js-password-eye-icon" />
                              </div>
                              <div className="input-wrapper js-input-wrapper">
                                <input
                                  type="checkbox"
                                  id="fruehstart_appbundle_userprofilerandregtype_terms"
                                  name="fruehstart_appbundle_userprofilerandregtype[terms]"
                                  required="required"
                                  className="js-register-terms"
                                  value={1}
                                />
                                <label
                                  className="small-label"
                                  htmlFor="fruehstart_appbundle_userprofilerandregtype_terms"
                                >
                                  <span>Ich stimme den </span>
                                  <a
                                    className="a-grey-undeline"
                                    href="/"
                                    target="_blank"
                                  >
                                    AGB
                                  </a>
                                  <span> zu und willige in die </span>
                                  <a href="/" data-toggle="collapse">
                                    Verarbeitung besonderer Kategorien
                                    personenbezogener Daten
                                  </a>
                                  <span> ein. Die </span>
                                  <a
                                    className="a-grey-undeline"
                                    href="/"
                                    target="_blank"
                                  >
                                    Datenschutzhinweise
                                  </a>
                                  <span> habe ich zur Kenntnis genommen.</span>
                                </label>
                              </div>
                              <div
                                className="personalDataExplaination-row collapse"
                                id="personalDataExplaination"
                              >
                                <span>
                                  Die von Dir im Zuge einer Bestellung
                                  angegebenen Daten können im Einzelfall
                                  Rückschlüsse auf Deinen Gesundheitszustand,
                                  Deine Herkunft oder Deine
                                  Religionszugehörigkeit zulassen. Wir verwenden
                                  diese Daten mit Deiner Einwilligung nur zu
                                  Zwecken der Vertragsdurchführung, d.h. um das
                                  am besten zu Dir passende Frühstück
                                  herauszufinden. Die Nichterteilung dieser
                                  Daten zieht keinerlei Nachteile für Dich nach
                                  sich. Du kannst Deine freiwillig erteilte
                                  Einwilligung jederzeit mit Wirkung für die
                                  Zukunft widerrufen.
                                </span>
                              </div>
                            </div>
                          </div>
                        </Step>
                      </Steps>
                    </div>
                  </CSSTransition>

                  <Navigation />
                </React.Fragment>
              )}
            />
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default Fragebogen;
