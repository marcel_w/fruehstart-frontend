import React, { Component } from "react";
import { Container } from "reactstrap";
import { Line, Circle } from "rc-progress";

class ProgressBar extends Component {
  state = {};
  render() {
    return (
      <Container className="justify-content-center mt-5">
        <Container>
          <header className="new-register-header">
            <div className="new-register-progress-container">
              <div className="progress-step-wrapper">
                <div className="progress-circle-wrapper">
                  <div>
                    <Circle
                      percent={this.props.circle1Progress}
                      strokeWidth="4"
                      strokeColor="#e73c52"
                      trailWidth="4"
                    />
                    <span className="progress-circle__percent mt-1">1</span>
                  </div>
                </div>
                <span className="progress-circle-subline">Lifestyle</span>
              </div>

              <div className="progress-step-wrapper mt-4 ml-2 mr-2">
                <div>
                  <Line
                    percent={this.props.line1Progress}
                    strokeWidth="4"
                    strokeColor="#e73c52"
                    trailWidth="4"
                  />
                </div>
              </div>

              <div className="progress-step-wrapper">
                <div className="progress-circle-wrapper">
                  <div>
                    <Circle
                      percent={this.props.circle2Progress}
                      strokeWidth="4"
                      strokeColor="#e73c52"
                      trailWidth="4"
                    />
                    <span className="progress-circle__percent mt-1">2</span>
                  </div>
                </div>
                <span className="progress-circle-subline">Essen</span>
              </div>

              <div className="progress-step-wrapper mt-4 ml-2 mr-2">
                <div>
                  <Line
                    percent={this.props.line2Progress}
                    strokeWidth="4"
                    strokeColor="#e73c52"
                    trailWidth="4"
                  />
                </div>
              </div>

              <div className="progress-step-wrapper">
                <div className="progress-circle-wrapper">
                  <div>
                    <Circle
                      percent={this.props.circle3Progress}
                      strokeWidth="4"
                      strokeColor="#e73c52"
                      trailWidth="4"
                    />
                    <span className="progress-circle__percent mt-1">3</span>
                  </div>
                </div>
                <span className="progress-circle-subline">Feinschliff</span>
              </div>
            </div>
          </header>
        </Container>
      </Container>
    );
  }
}

export default ProgressBar;
